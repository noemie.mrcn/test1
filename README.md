# Recommander System

## Table of content
   * [rec-sys](#section-train)
        * [About](#about)
        * [Project structure](#project-structure)
        * [Requirements](#requirements)
        * [Build](#build)
        * [Usage](#usage)
        * [Commands](#Commands)
            * [train](#train)
            * [export_datasets](#export_datasets)
            * [create_eval_dataset](#create_eval_dataset)
            * [predict_ratings](#predict_ratings)
        * [Tests](#tests)
        

## About 
This project aims to predict the rating of a user to a target movie based on user's previous rated movies.
To do so, my model is deeply inspired by Behavior Sequence Transformer for E-commerce
Recommendation in Alibaba [article](https://arxiv.org/pdf/1905.06874.pdf).

Recommender systems have also benefited from deep learning’s success. But usually, the baseline is to use an 
Embedding + MLP structure : raw features are embedded into lowdimensional vectors, which are then fed on to MLP for 
final recommendations.  
By doing so, the model doesn't take into account the sequential behaviour of the users in watching and rating movies.
The BST model leverages the sequential behaviour of the users in watching and rating movies, as well as user profile,
to predict the rating of the user to a target movie.

The BST model architecture in based on Transformer. For our recommendation problem, we replaced sequence of words with 
sequence of movies and applied NLP approaches. (Transformer is sucessful for tasks as Machine Translation).
The model implemented can be described as follow :
- previous rated movies are embedded into low-dimensional vector
- transformer layer
- MLP

## Project structure
recommander-sys is roughly structured as follows:
```
[rec-sys]
    [conf]
        params.json
    [src]
        [rec-sys]
            create_datasets.py
            data_generator.py
            file_utils.py
            model.py
            model_prediction.py
            parameters_validation.py
            train_model.py
    [tests]
        [resources]
        test_create_datasets.py
    main.py
    pyproject.toml
        ...
```
src should be defined as the source

`create_datasets.py` creates the train, test, validation and evaluation data set (in jsonl format)

`data_generator.py` generates data to feed the model

`file_utils.py` utils functions to read files

`model.py` model architecture

`model_prediction.py` predicts movies rating of the evaluation set based on the trained model

`parameters_validation.py` insures that the configuration file is well fulfilled

`train_model.py` trains the model

## Requirements

You need to have `poetry` installed. To install it, you need to run this CLI:

```
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

You also need to have a proper virtual environment defined for this project - the easiest way to follow the hereunder instructions:
```
$ pyenv virtualenv python-version name-of-your-environnement-in-python-version
$ pyenv activate name-of-your-environnement-in-python-version
```

Python version for this project is 3.7.1

## Build
- clone the project
- activate your dedicated virtual environment
- run the following command
```
$ poetry update
```

## Usage
In order to get the predicted rating on evaluation dataset, many steps should be executed : 
1) create the train, test, validation and evaluation datasets
2) train the model
3) predict rating to new data

The commands to execute the 3 steps are given below

## Commands
### train 
Trains the model with following parameters :

```
--config_path    path to configuration file
```
_Example :_ 

```
python main.py train --config_path path/conf_file.jsonl 
```

### export_datasets
Creates train, test, validation datasets in jsonl format with following parameters :

```
--train_path       path to output train dataset file
--test_path        path to output test dataset file
--validation_path  path to output validation dataset file
--ratings_path     path to csv ratings
```

_Example :_ 

```
python main.py export_datasets --train_path path/train_path.jsonl --test_path path/test_path.jsonl --validation_path path/validation_path.jsonl --ratings_path path/ratings_path.csv
```

### create_eval_dataset
Creates evaluation dataset in jsonl format with following parameters :

```
--eval_path           path to output eval dataset file
--eval_ratings_path   path to eval csv 
--ratings_path        path to csv ratings
```

_Example :_ 

```
python main.py create_eval_dataset --eval_path path/eval_path.jsonl --eval_ratings_path path/eval_ratings_path.jsonl --ratings_path path/ratings_path.csv
```

### predict_ratings
Predict rating to new data with following parameters :

```
--model_path        path to model file as json file
--weights_path      path to saved weights file as h5 file
--config_path       path to configuration file
--eval_ratings_path path to eval csv
```

_Example :_ 

```
python main.py predict_ratings --model_path path/model_path.jsonl --weights_path path/weights_path.h5 --config_path path/conf_file.jsonl --eval_ratings_path path/eval_path.jsonl
```

NB : A template of a configuration file with all parameters is available in the folder conf (`params.json`)

## Tests

To run all annotator tests: 
`$ make test`

To run individual test:
`$ pytest name_of_test.py`