import logging
import os
import typer

from rec_sys.create_datasets import csv_to_df, create_train_test_val_data, create_an_eval_dataset
from rec_sys.file_utils import load_config_file
from rec_sys.model_prediction import ModelPrediction
from rec_sys.train_model import train_model

app = typer.Typer()
logger = logging.getLogger()
logging.basicConfig(level=logging.INFO, format="%(message)s")


@app.command()
def train(
    config_path: str = typer.Argument(..., help="path to configuration file"),
):
    if os.path.exists(config_path):
        config = load_config_file(config_path)
        train_model(config)
    else:
        logger.error("The configuration path doesn't exist")


@app.command()
def export_datasets(
    train_path: str = typer.Argument(None, help="path to output train dataset file"),
    test_path: str = typer.Argument(None, help="path to output test dataset file"),
    validation_path: str = typer.Argument(None, help="path to output validation dataset file"),
    ratings_path: str = typer.Argument(..., help="path to csv ratings"),
):
    if not os.path.exists(ratings_path):
        logging.error("ratings file path does not exists at given path")
    else:
        df = csv_to_df(ratings_path)
        create_train_test_val_data(dataframe=df,
                                   output_train_json_path=train_path,
                                   output_test_json_path=test_path,
                                   output_val_json_path=validation_path)


@app.command()
def create_eval_dataset(
    eval_path: str = typer.Argument(None, help="path to output eval dataset file"),
    eval_ratings_path: str = typer.Argument(..., help="path to eval csv "),
    ratings_path: str = typer.Argument(..., help="path to csv ratings"),
):
    if os.path.exists(ratings_path):
        df = csv_to_df(ratings_path)
        create_an_eval_dataset(dataframe=df,
                               csv_path_eval=eval_ratings_path,
                               output_eval_json_path=eval_path)
    else:
        logger.error("ratings file path does not exists at given path")


@app.command()
def predict_ratings(
    model_path: str = typer.Argument(..., help="path to model file as json file"),
    weights_path: str = typer.Argument(..., help="path to saved weights file as h5 file"),
    config_path: str = typer.Argument(..., help="path to configuration file"),
    eval_ratings_path: str = typer.Argument(..., help="path to eval csv "),
):
    model_prediction = ModelPrediction(model_path, weights_path, config_path)
    model_prediction.predict_data(eval_ratings_path)

