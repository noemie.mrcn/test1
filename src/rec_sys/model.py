import logging
import math
import tensorflow as tf

from tensorflow.python.keras import Input, Model
from tensorflow.python.keras.layers import Embedding, Multiply, concatenate, MultiHeadAttention, Dropout, Add, \
    LayerNormalization, LeakyReLU, Dense, Flatten, Reshape, BatchNormalization
from tensorflow.python.keras.utils.vis_utils import plot_model

from typing import List


class RecommanderSystemModel:
    """
    Class implementing the RecommanderSystem model based on movie and movies
    rated just before by a user

    A transformer is applied on a sequence of movies.
    """

    def __init__(self,
                 user_size: int,
                 movie_size: int,
                 sequence_size: int,
                 target_size: int,
                 position_size: int,
                 num_heads: int,
                 dropout_rate: float,
                 hidden_units: List) -> None:
        """
        Constructor of class RecommanderSystemModel

        Args:
            user_size: number of users
            movie_size: number of movies
            sequence_size: size of the sequence movies
            target_size: size of the target movie
            position_size: size of the position vector
            num_heads: number of Attention heads
            dropout_rate: dropout rate
            hidden_units: number of hidden units in Dense Layer
        """
        self.user_size = user_size
        self.movie_size = movie_size
        self.sequence_size = sequence_size
        self.target_size = target_size
        self.position_size = position_size
        self.num_heads = num_heads
        self.dropout_rate = dropout_rate
        self.hidden_units = hidden_units

    def build_model(self):
        user_input = Input(shape=(self.target_size, ), dtype="int32", name="user_input")
        seq_movie_input = Input(shape=(self.sequence_size, ), dtype="int32", name="seq_movie_input")
        target_movie_input = Input(shape=(self.target_size, ), dtype="int32", name="target_movie_input")
        position_input = Input(shape=(self.target_size, ), dtype="int32", name="position_input")
        ratings_input = Input(shape=(self.sequence_size, self.target_size), dtype="float32", name="ratings_input")

        inputs = [user_input, seq_movie_input, target_movie_input, position_input, ratings_input]

        # user Embedding Layer
        user_embedding_dims = int(math.sqrt(self.user_size))
        user_embedding = Embedding(
            input_dim=self.user_size,
            output_dim=user_embedding_dims,
            name="user_embedding"
        )(user_input)

        # movie Embedding Layer
        movie_embedding_dims = int(math.sqrt(self.movie_size))
        movie_embedding = Embedding(
            input_dim=self.movie_size,
            output_dim=movie_embedding_dims,
            name="movie_embedding"
        )

        # target movie Embedding Layer
        target_movie_embedding = movie_embedding(target_movie_input)

        # sequence movie Embedding Layer
        seq_movie_embedding = movie_embedding(seq_movie_input)

        # position Embedding Layer
        position_embedding = Embedding(
            input_dim=self.position_size,
            output_dim=movie_embedding_dims,
            name="position_embedding"
        )(position_input)

        # sequence movie with position and rating Embedding Layer
        seq_movies_with_position_and_rating_embedding = Multiply()(
            [(seq_movie_embedding + position_embedding), ratings_input]
        )

        # Transformer features ==> target movie + sequence movie
        transformer_features = []
        for movie in tf.unstack(
                seq_movies_with_position_and_rating_embedding, axis=1
        ):
            transformer_features.append(tf.expand_dims(movie, 1))
        transformer_features.append(target_movie_embedding)

        encoded_transformer_features = concatenate(
            transformer_features, axis=1
        )

        # Multi-headed Attention Layer
        attention_output = MultiHeadAttention(
            num_heads=self.num_heads, key_dim=encoded_transformer_features.shape[2], dropout=self.dropout_rate
        )(encoded_transformer_features, encoded_transformer_features)

        # Transformer block
        attention_output = Dropout(self.dropout_rate)(attention_output)
        x1 = Add()([encoded_transformer_features, attention_output])
        x1 = LayerNormalization()(x1)
        x2 = LeakyReLU()(x1)
        x2 = Dense(units=x2.shape[-1])(x2)
        x2 = Dropout(self.dropout_rate)(x2)
        transformer_features = Add()([x1, x2])
        transformer_features = LayerNormalization()(transformer_features)
        features = Flatten()(transformer_features)

        features = concatenate(
                [features, Reshape([user_embedding.shape[-1]])(user_embedding)]
            )

        # Fully-connected Layers
        for num_units in self.hidden_units:
            features = Dense(num_units)(features)
            features = BatchNormalization()(features)
            features = LeakyReLU()(features)
            features = Dropout(self.dropout_rate)(features)

        outputs = Dense(units=1)(features)
        model = Model(inputs=inputs, outputs=outputs)
        logging.info("Model successfully built")

        # print of a string summary of the network
        model.summary()

        # graph plot of the model's architecture
        plot_model(model, show_shapes=True)

        return model
