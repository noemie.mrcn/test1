import json
import logging
import os
import numpy as np

from itertools import islice
from typing import Dict


class DataGenerator:
    """
    Generator to feed the RecommanderSystemModel
    with an infinite stream of data
    """

    def __init__(self,
                 dict_user_id: Dict,
                 dict_movie_id: Dict,
                 sequence_size: int,
                 target_size: int,
                 batch_size: int):
        """
        Constructor of class DataGenerator

        Args:
            dict_user_id: dict with as a key the id_user and as value the user
            dict_movie_id: dict with as a key the id_movie and as value the movie
            sequence_size: size of the sequence movies
            target_size: size of the target movie
            batch_size: batch size
        """
        self.dict_user_id = dict_user_id
        self.dict_movie_id = dict_movie_id
        self.sequence_size = sequence_size
        self.target_size = target_size
        self.batch_size = batch_size

    def generate(self, file_path: str, with_label: bool):
        """
        Generator for documents reading from a jsonl file

        Args:
            file_path: path to the jsonl to read from
            with_label: boolean specifying if those data are used for training the model

        Returns:
            Generates tuple of x input as dict, y input as vector
        """
        while True:
            counter = 0
            if not os.path.isfile(file_path):
                logging.log(level=40, msg=f"path {file_path} does not exists")
            else:
                with open(file_path, "r+", encoding="utf-8") as input_file:
                    for batch_lines in iter(lambda: tuple(islice(input_file, self.batch_size)), ()):
                        counter += 1
                        current_batch_size = len(batch_lines)
                        X_user = np.zeros((current_batch_size, self.target_size), dtype='int32')
                        X_seq_movie = np.zeros((current_batch_size, self.sequence_size), dtype='int32')
                        X_target_movie = np.zeros((current_batch_size, self.target_size), dtype='int32')
                        X_position = np.zeros((current_batch_size, self.sequence_size), dtype='int32')
                        X_ratings = np.zeros((current_batch_size, self.sequence_size), dtype='float64')
                        if with_label:
                            Y = np.zeros((current_batch_size, self.target_size), dtype='float64')

                        for i, line in enumerate(batch_lines):
                            failures = 0
                            try:
                                json_line = json.loads(line)
                            except json.JSONDecodeError:
                                failures += 1
                            if json_line:
                                X_user[i] = self.dict_user_id[json_line["userId"]]
                                X_seq_movie[i] = [self.dict_movie_id[movie] for movie in json_line["seq_movie"]]
                                X_target_movie[i] = self.dict_movie_id[json_line["movieId"]]
                                X_position[i] = list(range(self.sequence_size))
                                X_ratings[i] = json_line["seq_rating"]
                                if with_label:
                                    Y[i] = json_line["label"]
                        if with_label:
                            yield ({'user_input': X_user,
                                    'seq_movie_input': X_seq_movie,
                                    'target_movie_input': X_target_movie,
                                    'position_input': X_position,
                                    'ratings_input': X_ratings},
                                   Y)
                        else:
                            yield {'user_input': X_user,
                                   'seq_movie_input': X_seq_movie,
                                   'target_movie_input': X_target_movie,
                                   'position_input': X_position,
                                   'ratings_input': X_ratings}
