import math
from typing import List

import pandas as pd

from rec_sys.create_datasets import get_dict_movie_id, get_dict_user_id
from rec_sys.data_generator import DataGenerator
from rec_sys.file_utils import load_model_from_json_and_weights, load_config_file


class ModelPrediction:

    """
    Model Prediction class that loads a model and use it to make new predictions
    on a series of data
    """
    def __init__(self, model_path: str, weights_path: str, config_path: str) -> None:
        """
        Constructor of class ModelPrediction

        Args:
            model_path: path to saved model architecture as json file
            weights_path: path to saved weights as h5 or hdf5 file
            config_path: path to config
        """
        self.model = load_model_from_json_and_weights(model_path, weights_path)
        self.config = load_config_file(config_path)

    def predict_data(self, eval_csv_path: str):
        """
        Predict the rating of a movie
        """
        dict_movie_id = get_dict_movie_id(self.config["csv_ratings_path"])
        dict_user_id = get_dict_user_id(self.config["csv_ratings_path"])
        data_generator = DataGenerator(dict_user_id=dict_user_id,
                                       dict_movie_id=dict_movie_id,
                                       sequence_size=self.config["sequence_size"],
                                       target_size=self.config["target_size"],
                                       batch_size=self.config["batch_size"])
        labels = self.model.predict(
            data_generator.generate(file_path=self.config["eval_data_set"], with_label=False),
            batch_size=self.config["batch_size"],
            steps=math.ceil(self.config["eval_set_size"]/self.config["batch_size"]),
        )
        labels = [rating[0] for rating in labels.tolist()]
        self.write_predictions_into_csv(labels, eval_csv_path)

    @staticmethod
    def write_predictions_into_csv(labels: List, eval_csv_path):
        """
        Write predictions into the evaluation csv file
        """
        df_eval = pd.read_csv(eval_csv_path)
        df_eval["rating"] = labels
        df_eval[["userId", "movieId", "rating"]].to_csv(eval_csv_path)
