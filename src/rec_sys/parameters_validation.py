import itertools
import logging

from tabulate import tabulate
from typing import Dict


class ParametersValidation:
    def __init__(self,
                 param: Dict,
                 logger: logging.Logger) -> None:
        self.param = param
        self.logger = logger

    def validate_param(self) -> None:
        logging.basicConfig(level=logging.INFO, format="%(message)s")
        if "training_data_set" not in self.param:
            logging.log(level=40, msg=f"The training dataset path is missing into the provided model param")
        if "validation_data_set" not in self.param:
            logging.log(level=40, msg=f"The validation dataset path is missing into the provided model param")
        if "test_data_set" not in self.param:
            logging.log(level=40, msg=f"The test dataset path is missing into the provided model param")
        if "eval_data_set" not in self.param:
            logging.log(level=40, msg=f"The evaluation dataset path is missing into the provided model param")
        if "csv_ratings_path" not in self.param:
            logging.log(level=40, msg=f"The ratings csv path is missing into the provided model param")
        if "training_set_size" not in self.param:
            logging.log(
                level=40,
                msg=f"The provided model param do not contain the number of documents in the training set",
            )
        if "validation_set_size" not in self.param:
            logging.log(
                level=40,
                msg=f"The provided model param do not contain the number of documents in the validation set",
            )
        if "test_set_size" not in self.param:
            logging.log(
                level=40,
                msg=f"The provided model param do not contain the number of documents in the test set",
            )
        if "eval_set_size" not in self.param:
            logging.log(
                level=40,
                msg=f"The provided model param do not contain the number of documents in the evaluation set",
            )
        if "sequence_size" not in self.param:
            logging.log(
                level=40, msg=f"The provided model param do not contain the length of a sequence of movies"
            )
        if "target_size" not in self.param:
            logging.log(
                level=40, msg=f"The provided model param do not contain the length of the target movie"
            )
        if "position_size" not in self.param:
            logging.log(
                level=40, msg=f"The provided model param do not contain the length of the position vector"
            )
        if "user_size" not in self.param:
            logging.log(level=40, msg=f"The provided model param do not contain the number of users present "
                                      f"in the database")
        if "movie_size" not in self.param:
            logging.log(level=40, msg=f"The provided model param do not contain the number of movies present "
                                      f"in the database")
        if "learning_rate" not in self.param:
            logging.log(
                level=40, msg=f"The provided model param do not contain the learning rate for the loss function"
            )
        if "epochs" not in self.param:
            logging.log(level=40, msg=f"The provided model param do not contain the number of epochs")
        if "batch_size" not in self.param:
            logging.log(level=40, msg=f"The provided model param do not contain the loaded size of the batch")
        if "num_heads" not in self.param:
            logging.log(
                level=40, msg=f"The provided model param do not contain the number of heads for attention layer"
            )
        if "dropout_rate" not in self.param:
            logging.log(level=40, msg=f"The provided model param do not contain the dropout rate")
        if "hidden_state" not in self.param:
            logging.log(level=40,
                        msg=f"The provided model param do not contain the shape of the hidden_state")
        if "log_dir" not in self.param:
            logging.log(level=40, msg=f"The log directory is missing into the provided model param")
        if "model_dir" not in self.param:
            logging.log(
                level=40, msg=f"The provided model param do not contain the model directory to save results"
            )

        param_element = [['training_data_set', 'validation_data_set', 'test_data_set', 'eval_data_set'],
                         ['csv_ratings_path'], ['training_set_size', 'validation_set_size', 'test_set_size',
                         'eval_set_size'],
                         ['sequence_size', 'target_size', 'position_size', 'user_size', 'movie_size'],
                         ['learning_rate', 'epochs', 'batch_size', 'num_heads', 'dropout_rate', 'hidden_state'],
                         ['model_dir', 'log_dir']]

        if all(param in self.param for param in list(itertools.chain(*param_element))):
            for k, elements in enumerate(param_element):
                data = [self.param[element] for element in elements]
                self.logger.info(tabulate([data], param_element[k], tablefmt="grid"))