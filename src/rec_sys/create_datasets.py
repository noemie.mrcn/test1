import itertools
import numpy as np
import pandas as pd

from more_itertools import windowed
from typing import Dict, List, Tuple, TypeVar

PandasDataFrame = TypeVar("pandas.core.frame.DataFrame")


def csv_to_df(csv_path: str) -> PandasDataFrame:
    """
    Read a comma-separated values (csv) file into DataFrame
    """
    df = pd.read_csv(csv_path)
    return df


def extract_user_movie_rating(df: PandasDataFrame) -> Tuple:
    """
    Sort the dataframe by id by date and output the list of user_id,
    the list of movie (rated chronologically) by user_id and
    the list of ratings associated by user_id
    """
    df["rank_latest"] = df.groupby(["userId"])["timestamp"].rank(method="first", ascending=False)
    df.sort_values(by=['userId', 'rank_latest'], inplace=True, ascending=[True, False])

    m_id = list(df.groupby("userId")["movieId"].apply(list))
    r_id = list(df.groupby("userId")["rating"].apply(list))
    u_id = list(df["userId"].unique())

    return m_id, r_id, u_id


def get_dict_movie_id(csv_path: str) -> Dict:
    """
    Get the correspondence between a movie and its id
    """
    df = pd.read_csv(csv_path)
    unique_m_id = list(df["movieId"].unique())
    dict_movie_id = {k: i + 1 for (i, k) in enumerate(sorted(unique_m_id))}
    dict_movie_id[0] = 0
    return dict_movie_id


def get_dict_user_id(csv_path: str) -> Dict:
    """
    Get the correspondence between a user and its id
    """
    df = pd.read_csv(csv_path)
    unique_u_id = list(df["userId"].unique())
    dict_user_id = {k: k - 1 for k in unique_u_id}
    return dict_user_id


def sliced_id(list_id: List, n: int, step: int) -> List:
    """
    Split a list of element (movies or ratings) into a set of sequence of a fixed length
    :param list_id:
    :param n: length of the sequence
    :param step: control the number of sequences to generate for id
    :return: a list of list, each sublist is a sequence of size n
    """
    dataset = []

    for element in list_id:
        len_list = len(element)
        # padding
        if len_list <= 4:
            sliced_m_id = [[0] * (4 - len_list) + element]

        # slicing
        else:
            sliced_m_id = list(windowed(element, n=n, step=step))
            sliced_m_id = [list(elem) for elem in sliced_m_id]
            if len_list % 2 != 0:
                last = sliced_m_id[-1]
                for i in reversed(range(3)):
                    last[i + 1] = last[i]
                last[0] = sliced_m_id[-2][1]
        dataset.append(sliced_m_id)
    return dataset


def sliced_movie_rating_user(df: PandasDataFrame) -> Tuple:
    """
    Split the movie ids and ratings ids list into a set of sequences of a fixed length and
    associate a user id for each sublist
    """
    m_id, r_id, u_id = extract_user_movie_rating(df)
    movie = sliced_id(m_id, n=4, step=2)
    rating = sliced_id(r_id, n=4, step=2)
    user = [[u] * len(r) for u, r in zip(u_id, rating)]
    return movie, rating, user


def create_an_eval_dataset(dataframe: PandasDataFrame, csv_path_eval: str, output_eval_json_path: str) -> None:
    """
    Create the jsonl for evaluation
    """
    d_movie, d_rating = {}, {}
    df_eval = pd.read_csv(csv_path_eval)
    u_id = list(dataframe["userId"].unique())
    movie, rating, user = sliced_movie_rating_user(dataframe)
    for u in u_id:
        d_movie[u] = movie[u - 1][-1][1:]
        d_rating[u] = rating[u - 1][-1][1:]
    df_eval["seq_movie"] = df_eval["userId"].map(d_movie)
    df_eval["seq_rating"] = df_eval["userId"].map(d_rating)
    df_eval.to_json(output_eval_json_path, orient="records", lines=True)


def create_train_test_val_data(dataframe: PandasDataFrame,
                               output_train_json_path: str,
                               output_test_json_path: str,
                               output_val_json_path: str) -> None:
    """
    Create the jsonl for the train, the test and the validation data sets
    """
    movie, rating, user = sliced_movie_rating_user(dataframe)
    movie_list, rating_list, user_list = list(itertools.chain(*movie)), list(itertools.chain(*rating)), \
                                         list(itertools.chain(*user))

    data = {'userId': user_list, 'seq_movie': movie_list, 'seq_rating': rating_list}
    dataset = pd.DataFrame(data)
    dataset["movieId"] = dataset["seq_movie"].apply(lambda x: x[-1])
    dataset["seq_movie"] = dataset["seq_movie"].apply(lambda x: x[:-1])
    dataset["label"] = dataset["seq_rating"].apply(lambda x: x[-1])
    dataset["seq_rating"] = dataset["seq_rating"].apply(lambda x: x[:-1])

    random_selection_80 = np.random.rand(len(dataset.index)) <= 0.80
    data = dataset[random_selection_80]
    test_data = dataset[~random_selection_80]

    random_selection_90 = np.random.rand(len(data.index)) <= 0.90
    train_data = data[random_selection_90]
    validation_data = data[~random_selection_90]

    train_data.to_json(output_train_json_path, orient="records", lines=True)
    test_data.to_json(output_test_json_path, orient="records", lines=True)
    validation_data.to_json(output_val_json_path, orient="records", lines=True)

