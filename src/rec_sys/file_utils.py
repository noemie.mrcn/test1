import io
import json
import logging
import os
from typing import Dict

from tensorflow import keras
from tensorflow.python.keras import Sequential


def load_config_file(config_path: str) -> Dict:
    """
    Load a config, provided path to json file
    """
    if not os.path.isfile(config_path):
        logging.log(level=40, msg=f"path {config_path} does not exists")
    else:
        with io.open(config_path, mode="r", encoding="utf8") as config_file:
            config = json.load(config_file)
        return config


def load_model_from_json_and_weights(model_path: str, weights_path: str) -> Sequential:
    """
    Load a model, provided path to model json and h5 weights
    """
    with open(model_path, "r") as json_file:
        json_string = json_file.read()
    model = keras.models.model_from_json(json_string)
    model.load_weights(weights_path)
    return model
