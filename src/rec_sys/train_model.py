import logging
import math
import matplotlib.pyplot as plt

from typing import Dict, Tuple

import tensorflow as tf
from tensorflow.python.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau

from rec_sys.create_datasets import get_dict_movie_id, get_dict_user_id
from rec_sys.data_generator import DataGenerator
from rec_sys.model import RecommanderSystemModel
from rec_sys.parameters_validation import ParametersValidation


def train_model(params: Dict) -> Tuple:
    """
    Train the model.
    """

    logging.basicConfig(level=logging.INFO, format="%(message)s")

    # Check the parameters are loaded and available in the params dict.
    logger = logging.getLogger()
    params_check = ParametersValidation(param=params, logger=logger)
    params_check.validate_param()

    dict_movie_id = get_dict_movie_id(params["csv_ratings_path"])
    dict_user_id = get_dict_user_id(params["csv_ratings_path"])

    # Load model
    recommander_system = RecommanderSystemModel(
        user_size=params["user_size"],
        movie_size=params["movie_size"],
        sequence_size=params["sequence_size"],
        target_size=params["target_size"],
        position_size=params["position_size"],
        num_heads=params["num_heads"],
        dropout_rate=params["dropout_rate"],
        hidden_units=params["hidden_state"]
    )
    model = recommander_system.build_model()

    # Set loss, optimizer and metric
    model.compile(
        optimizer=tf.keras.optimizers.Adagrad(learning_rate=params["learning_rate"]),
        loss=tf.keras.losses.MeanSquaredError(),
        metrics=[tf.keras.metrics.RootMeanSquaredError()])
    logging.info("RecommanderSystemModel built:")

    data_generator = DataGenerator(dict_user_id=dict_user_id,
                                   dict_movie_id=dict_movie_id,
                                   sequence_size=params["sequence_size"],
                                   target_size=params["target_size"],
                                   batch_size=params["batch_size"])

    # Define callbacks
    checkpoint = ModelCheckpoint(params["model_dir"] + '/weights.{epoch:02d}.hdf5', verbose=1)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss',
                                  factor=0.5,
                                  patience=1,
                                  verbose=1,
                                  mode='auto',
                                  epsilon=0.0001,
                                  cooldown=0,
                                  min_lr=0)
    callbacks = [checkpoint, reduce_lr]

    # Save model
    with open(params["model_dir"] + "/model_result.json", "w") as json_file:
        json_file.write(model.to_json())

    # Fit model
    logging.info("Start training")

    history = model.fit(
        data_generator.generate(file_path=params["training_data_set"], with_label=True),
        epochs=params["epochs"],
        batch_size=params["batch_size"],
        steps_per_epoch=math.ceil(params["training_set_size"]/params["batch_size"]),
        validation_data=data_generator.generate(file_path=params["validation_data_set"], with_label=True),
        validation_steps=math.ceil(params["validation_set_size"]/params["batch_size"]),
        callbacks=callbacks
    )

    # Print results
    history = history.history
    logging.info(history)
    plot_accuracy_loss_train_validation(history, params)
    model.save_weights(params["model_dir"] + "/model_result_weights.h5")

    # Evaluation
    logging.info("Start evaluating on test set")
    _, rmse = model.evaluate(
        data_generator.generate(file_path=params["test_data_set"], with_label=True),
        steps=math.ceil(params["test_set_size"]/params["batch_size"]),
        batch_size=params["batch_size"])

    logging.info("Test RMSE: {}".format(round(rmse, 3)))

    return history['val_root_mean_squared_error'], history['val_loss'], round(rmse, 3)


def plot_accuracy_loss_train_validation(history: Dict, params: Dict):
    # summarize history for accuracy
    plt.plot(range(1, params["epochs"] + 1), history["root_mean_squared_error"])
    plt.plot(range(1, params["epochs"] + 1), history["val_root_mean_squared_error"])
    plt.title("model accuracy")
    plt.ylabel("accuracy")
    plt.xlabel("epoch")
    plt.legend(["train", "val"], loc="upper left")
    plt.savefig(params["log_dir"] + "/rmse.png")
    plt.close()

    # summarize history for loss
    plt.plot(range(1, params["epochs"] + 1), history["loss"])
    plt.plot(range(1, params["epochs"] + 1), history["val_loss"])
    plt.title("model loss")
    plt.ylabel("loss")
    plt.xlabel("epoch")
    plt.legend(["train", "val"], loc="upper left")
    plt.savefig(params["log_dir"] + "/loss.png")




