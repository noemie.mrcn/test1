import itertools
import os

import pandas as pd
import pytest

from rec_sys.create_datasets import extract_user_movie_rating, sliced_movie_rating_user
from tests.resources.settings import RESOURCES_DIR

RATING_CSV_PATH = os.path.join(RESOURCES_DIR, "sample_ratings.csv")
DATA = pd.read_csv(RATING_CSV_PATH)


@pytest.mark.skipif(
    condition=not os.path.exists(RATING_CSV_PATH), reason="Resource file not present for the tests"
)
def test_equal_length_after_sorting_data():
    movie, rating, user = extract_user_movie_rating(DATA)
    all_movie = list(itertools.chain(*movie))
    all_rating = list(itertools.chain(*rating))
    assert len(movie) == len(rating) == len(user)
    assert len(all_movie) == len(all_rating)


@pytest.mark.skipif(
    condition=not os.path.exists(RATING_CSV_PATH), reason="Resource file not present for the tests"
)
def test_slicing_into_sequence():
    movie, rating, user = sliced_movie_rating_user(DATA)
    flatten_movie = list(itertools.chain(*movie))
    flatten_rating = list(itertools.chain(*rating))
    concat_movie = list(itertools.chain(*flatten_movie))
    concat_rating = list(itertools.chain(*flatten_rating))
    movie_seq = [len(k) for element in movie for k in element]
    rating_seq = [len(k) for element in rating for k in element]
    assert all(x == 4 for x in movie_seq)
    assert all(x == 4 for x in rating_seq)
    assert len(movie) == len(rating) == len(movie)
    assert len(flatten_movie) == len(flatten_rating)
    assert len(concat_movie) == len(concat_rating)
    assert None not in concat_movie
    assert None not in concat_rating


